int LEDPIN1=0;
int LEDPIN2=2;
int Button=5;
int ButtonState=0;
int num=0;
void setup() {
  pinMode(LEDPIN1,  OUTPUT);
  pinMode(LEDPIN2,  OUTPUT);
  pinMode(Button,INPUT);
}

void loop() {
  ButtonState=digitalRead(Button);
  if (ButtonState==HIGH)
  {
    digitalWrite(LEDPIN1, LOW);
    digitalWrite(LEDPIN2, LOW);
    if (num==1)
    {
      num=0;
    }
    else
    {
      num=1;
    }
    delay(500);
  }
  
  if (num==1)
  {
    
    digitalWrite(LEDPIN1, HIGH);
    
  }
  else
  {
    digitalWrite(LEDPIN2, HIGH);
  }

}
