/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.cpp																			  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#include "MainWindow.h"
#include "Game.h"

Game::Game( MainWindow& wnd )
	:
	wnd( wnd ),
	gfx( wnd )
{
}

void Game::Go()
{
	gfx.BeginFrame();	
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel()
{
	if (wnd.kbd.KeyIsPressed(VK_UP) && y_Mobile - 5>0)
	{
		y_Mobile -= 1;
	}
	if (wnd.kbd.KeyIsPressed(VK_DOWN) && y_Mobile + 10<=gfx.ScreenHeight)
	{
		y_Mobile += 1;
	}

	if (wnd.kbd.KeyIsPressed(VK_RIGHT))
	{
		x_Mobile += 1;
	}
	if (wnd.kbd.KeyIsPressed(VK_LEFT))
	{
		x_Mobile -= 1;
	}
}

void Game::ComposeFrame()
{
	Cursor(x_Mobile, y_Mobile, 000, 255, 000);
	Head();
}

void Game::Cursor(int x,int y,int r,int g, int b)
{
	gfx.PutPixel(x    , y, r, g, b);

	gfx.PutPixel(x + 1, y + 1, r, g, b);
	gfx.PutPixel(x + 2, y + 2, r, g, b);
	gfx.PutPixel(x + 3, y + 3, r, g, b);
	gfx.PutPixel(x + 4, y + 4, r, g, b);
	gfx.PutPixel(x + 5, y + 5, r, g, b);

	gfx.PutPixel(x + 1, y -+1, r, g, b);
	gfx.PutPixel(x + 2, y -+2, r, g, b);
	gfx.PutPixel(x + 3, y -+3, r, g, b);
	gfx.PutPixel(x + 4, y -+4, r, g, b);
	gfx.PutPixel(x + 5, y -+5, r, g, b);
					    
	gfx.PutPixel(x - 1, y + 1, r, g, b);
	gfx.PutPixel(x - 2, y + 2, r, g, b);
	gfx.PutPixel(x - 3, y + 3, r, g, b);
	gfx.PutPixel(x - 4, y + 4, r, g, b);
	gfx.PutPixel(x - 5, y + 5, r, g, b);

	gfx.PutPixel(x - 1, y - 1, r, g, b);
	gfx.PutPixel(x - 2, y - 2, r, g, b);
	gfx.PutPixel(x - 3, y - 3, r, g, b);
	gfx.PutPixel(x - 4, y - 4, r, g, b);
	gfx.PutPixel(x - 5, y - 5, r, g, b);

}

void Game::Head()
{
	for (int x; x<gfx.ScreenWidth - 1;x++)
	{
		gfx.PutPixel(x, 3, 255, 255, 000);
		gfx.PutPixel(x, 14, 255, 255, 000);
	}
	for (int x; x < gfx.ScreenWidth; x+=100)
	{
		for (int y = 3; y < 14; y++)
		{
			gfx.PutPixel(x, y, 255, 255, 000);
		}
	}

}