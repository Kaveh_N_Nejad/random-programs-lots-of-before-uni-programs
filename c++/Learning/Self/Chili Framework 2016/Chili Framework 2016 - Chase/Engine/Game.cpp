/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.cpp																			  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#include "MainWindow.h"
#include "Game.h"

Game::Game(MainWindow& wnd)
	:
	wnd( wnd ),
	gfx( wnd )
{
}

void Game::Go()
{
	gfx.BeginFrame();	
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel()
{
	if (wnd.kbd.KeyIsPressed(VK_UP))
	{
		Player_x -= 2;
	}
	if (wnd.kbd.KeyIsPressed(VK_DOWN))
	{
		Player_x += 2;
	}
	if (wnd.kbd.KeyIsPressed(VK_LEFT))
	{
		Player_y -= 2;
	}
	if (wnd.kbd.KeyIsPressed(VK_RIGHT))
	{
		Player_y += 2;
	}


	if (Enemy_x > Player_x)
	{
		Enemy_x -= 1;
	}
	else
	{
		Enemy_x += 1;
	}

	if (Enemy_y > Player_y)
	{
		Enemy_y -= 1;
	}
	else
	{
		Enemy_y+= 1;
	}



	if (Enemy_2_x > Player_x)
	{
		Enemy_2_x -= 1;
	}
	else
	{
		Enemy_2_x += 1;
	}

	if (Enemy_2_y > Player_y)
	{
		Enemy_2_y -= 1;
	}
	else
	{
		Enemy_2_y += 1;
	}


	if (Enemy_3_x > Player_x)
	{
		Enemy_3_x -= 1;
	}
	else
	{
		Enemy_3_x += 1;
	}

	if (Enemy_3_y > Player_y)
	{
		Enemy_3_y -= 1;
	}
	else
	{
		Enemy_3_y += 1;
	}


	//lose
	if (Player_x == Enemy_x && Player_y == Enemy_y)
	{
		Play = false;
	}
	if (Player_x == Enemy_2_x && Player_y == Enemy_2_y)
	{
		Play = false;
	}
	if (Player_x == Enemy_3_x && Player_y == Enemy_3_y)
	{
		Play = false;
	}

	//win
	if ((Enemy_x == Enemy_2_x && Enemy_y == Enemy_2_y) && (Enemy_x == Enemy_3_x && Enemy_y == Enemy_3_y))
	{
		Play = false;
	}
	
}

void Game::ComposeFrame()
{
	if (Play)
	{
		Main_Player(Player_x, Player_y);
		Enemy(Enemy_x, Enemy_y);
		Enemy_2(Enemy_2_x, Enemy_2_y);
		Enemy_2(Enemy_3_x, Enemy_3_y);
	}
}
void Game::Main_Player(int Player_y, int Player_x)
{
	gfx.PutPixel(Player_x + 0, Player_y + 0, 255, 255, 255);

	gfx.PutPixel(Player_x + 1, Player_y + 0, 255, 255, 255);
	gfx.PutPixel(Player_x + 2, Player_y + 0, 255, 255, 255);
	gfx.PutPixel(Player_x + 3, Player_y + 0, 255, 255, 255);
	gfx.PutPixel(Player_x + 4, Player_y + 0, 255, 255, 255);
	gfx.PutPixel(Player_x + 5, Player_y + 0, 255, 255, 255);

	gfx.PutPixel(Player_x + 0, Player_y - 1, 255, 255, 255);
	gfx.PutPixel(Player_x + 0, Player_y - 2, 255, 255, 255);
	gfx.PutPixel(Player_x + 0, Player_y - 3, 255, 255, 255);
	gfx.PutPixel(Player_x + 0, Player_y - 4, 255, 255, 255);
	gfx.PutPixel(Player_x + 0, Player_y - 5, 255, 255, 255);

	gfx.PutPixel(Player_x + 5, Player_y - 1, 255, 255, 255);
	gfx.PutPixel(Player_x + 5, Player_y - 2, 255, 255, 255);
	gfx.PutPixel(Player_x + 5, Player_y - 3, 255, 255, 255);
	gfx.PutPixel(Player_x + 5, Player_y - 4, 255, 255, 255);
	gfx.PutPixel(Player_x + 5, Player_y - 5, 255, 255, 255);

	gfx.PutPixel(Player_x + 1, Player_y - 5, 255, 255, 255);
	gfx.PutPixel(Player_x + 2, Player_y - 5, 255, 255, 255);
	gfx.PutPixel(Player_x + 3, Player_y - 5, 255, 255, 255);
	gfx.PutPixel(Player_x + 4, Player_y - 5, 255, 255, 255);
	gfx.PutPixel(Player_x + 5, Player_y - 5, 255, 255, 255);
}
void Game::Enemy(int Enemy_y , int Enemy_x)
{

	gfx.PutPixel(Enemy_x + 0, Enemy_y + 0, 255, 255, 255);
			
	gfx.PutPixel(Enemy_x + 1, Enemy_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 2, Enemy_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 3, Enemy_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 4, Enemy_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 5, Enemy_y + 0, 255, 255, 255);
				
	gfx.PutPixel(Enemy_x + 0, Enemy_y - 1, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 0, Enemy_y - 2, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 0, Enemy_y - 3, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 0, Enemy_y - 4, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 0, Enemy_y - 5, 255, 255, 255);
				
	gfx.PutPixel(Enemy_x + 5, Enemy_y - 1, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 5, Enemy_y - 2, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 5, Enemy_y - 3, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 5, Enemy_y - 4, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 5, Enemy_y - 5, 255, 255, 255);

	gfx.PutPixel(Enemy_x + 1, Enemy_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 2, Enemy_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 3, Enemy_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 4, Enemy_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_x + 5, Enemy_y - 5, 255, 255, 255);
}
void Game::Enemy_2(int Enemy_2_y, int Enemy_2_x)
{

	gfx.PutPixel(Enemy_2_x + 0, Enemy_2_y + 0, 255, 255, 255);
					   
	gfx.PutPixel(Enemy_2_x + 1, Enemy_2_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 2, Enemy_2_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 3, Enemy_2_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 4, Enemy_2_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 5, Enemy_2_y + 0, 255, 255, 255);
					   
	gfx.PutPixel(Enemy_2_x + 0, Enemy_2_y - 1, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 0, Enemy_2_y - 2, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 0, Enemy_2_y - 3, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 0, Enemy_2_y - 4, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 0, Enemy_2_y - 5, 255, 255, 255);

	gfx.PutPixel(Enemy_2_x + 5, Enemy_2_y - 1, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 5, Enemy_2_y - 2, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 5, Enemy_2_y - 3, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 5, Enemy_2_y - 4, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 5, Enemy_2_y - 5, 255, 255, 255);
		
	gfx.PutPixel(Enemy_2_x + 1, Enemy_2_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 2, Enemy_2_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 3, Enemy_2_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 4, Enemy_2_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_2_x + 5, Enemy_2_y - 5, 255, 255, 255);
}
void Game::Enemy_3(int Enemy_3_y, int Enemy_3_x)
{

	gfx.PutPixel(Enemy_3_x + 0, Enemy_3_y + 0, 255, 255, 255);
									  
	gfx.PutPixel(Enemy_3_x + 1, Enemy_3_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 2, Enemy_3_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 3, Enemy_3_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 4, Enemy_3_y + 0, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 5, Enemy_3_y + 0, 255, 255, 255);
					   			  
	gfx.PutPixel(Enemy_3_x + 0, Enemy_3_y - 1, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 0, Enemy_3_y - 2, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 0, Enemy_3_y - 3, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 0, Enemy_3_y - 4, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 0, Enemy_3_y - 5, 255, 255, 255);
					
	gfx.PutPixel(Enemy_3_x + 5, Enemy_3_y - 1, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 5, Enemy_3_y - 2, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 5, Enemy_3_y - 3, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 5, Enemy_3_y - 4, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 5, Enemy_3_y - 5, 255, 255, 255);
					
	gfx.PutPixel(Enemy_3_x + 1, Enemy_3_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 2, Enemy_3_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 3, Enemy_3_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 4, Enemy_3_y - 5, 255, 255, 255);
	gfx.PutPixel(Enemy_3_x + 5, Enemy_3_y - 5, 255, 255, 255);
}