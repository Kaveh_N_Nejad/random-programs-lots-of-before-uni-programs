/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.cpp																			  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#include "MainWindow.h"
#include "Game.h"

Game::Game( MainWindow& wnd )
	:
	wnd( wnd ),
	gfx( wnd )
{
}

void Game::Go()
{
	gfx.BeginFrame();	
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel()
{
	if (wnd.kbd.KeyIsPressed(VK_RIGHT))
	{
		if (inhibitRight)
		{

		}
		else
		{
			vx = vx + 1;
			inhibitRight = true;
		}
	}
	else

	{
		inhibitRight = false;
	}

	if (wnd.kbd.KeyIsPressed(VK_LEFT))
	{
		if (inhibitLeft)
		{

		}
		else
		{
			vx = vx - 1;
			inhibitLeft = true;
		}
	}
	else

	{
		inhibitLeft = false;
	}

	if (wnd.kbd.KeyIsPressed(VK_DOWN))
	{
		if (inhibitDown)
		{

		}
		else
		{
			vy = vy + 1;
			inhibitDown = true;
		}
	}
	else

	{
		inhibitDown = false;
	}

	if (wnd.kbd.KeyIsPressed(VK_UP))
	{
		if (inhibitUp)
		{

		}
		else
		{
			vy = vy - 1;
			inhibitUp = true;
		}
	}
	else

	{
		inhibitUp= false;
	}

	x_mobile =x_mobile + vx;
	y_mobile =y_mobile+ vy;



	ShiftIsPressed = false;
	colliding = false;

	ShiftIsPressed = wnd.kbd.KeyIsPressed(VK_SHIFT);

	colliding = 
	OverLapTest(x_fixed0, y_fixed0, x_mobile, y_mobile)||
	OverLapTest(x_fixed1, y_fixed1, x_mobile, y_mobile)||
	OverLapTest(x_fixed2, y_fixed2, x_mobile, y_mobile)||
	OverLapTest(x_fixed3, y_fixed3, x_mobile, y_mobile);

	
	x_mobile=ClampScreenx(x_mobile);

	y_mobile = ClampScreeny(y_mobile);
	

}

void Game::ComposeFrame()
{
	DrawBox(x_fixed0,y_fixed0,000,255,000);
	DrawBox(x_fixed1, y_fixed1, 000, 255, 000);
	DrawBox(x_fixed2, y_fixed2, 000, 255, 000);
	DrawBox(x_fixed3, y_fixed3, 000, 255, 000);

	
	 

	if (colliding)
	{
		DrawBox(x_mobile, y_mobile, 255, 000, 000);
	}
	else
	{
		DrawBox(x_mobile, y_mobile, 255, 255, 255);
	}

}

void Game::DrawBox(int x, int y, int r, int g, int b)
{
	gfx.PutPixel(-5 + x, -5 + y, r, g, b);
	gfx.PutPixel(-5 + x, -4 + y, r, g, b);
	gfx.PutPixel(-5 + x, -3 + y, r, g, b);
	gfx.PutPixel(-4 + x, -5 + y, r, g, b);
	gfx.PutPixel(-3 + x, -5 + y, r, g, b);
	gfx.PutPixel(-5 + x, 5 + y, r, g, b);
	gfx.PutPixel(-5 + x, 4 + y, r, g, b);
	gfx.PutPixel(-5 + x, 3 + y, r, g, b);
	gfx.PutPixel(-4 + x, 5 + y, r, g, b);
	gfx.PutPixel(-3 + x, 5 + y, r, g, b);
	gfx.PutPixel(5 + x, -5 + y, r, g, b);
	gfx.PutPixel(5 + x, -4 + y, r, g, b);
	gfx.PutPixel(5 + x, -3 + y, r, g, b);
	gfx.PutPixel(4 + x, -5 + y, r, g, b);
	gfx.PutPixel(3 + x, -5 + y, r, g, b);
	gfx.PutPixel(5 + x, 5 + y, r, g, b);
	gfx.PutPixel(5 + x, 4 + y, r, g, b);
	gfx.PutPixel(5 + x, 3 + y, r, g, b);
	gfx.PutPixel(4 + x, 5 + y, r, g, b);
	gfx.PutPixel(3 + x, 5 + y, r, g, b);


}
bool Game::OverLapTest(int box0x, int box0y, int box1x, int box1y)
{
	
	const int left_Box0 = box0x - 5;
	const int right_Box0 = box0x + 5;
	const int top_Box0 = box0y - 5;
	const int bottom_Box0 = box0y + 5;

	const int left_box1 = box1x - 5;
	const int right_box1 = box1x + 5;
	const int top_box1 = box1y - 5;
	const int bottom_box1 = box1y + 5;

	return
		left_Box0 <= right_box1 &&
		right_Box0 >= left_box1 &&
		top_Box0 <= bottom_box1 &&
		bottom_Box0 >= top_box1;
	
}
int Game::ClampScreenx(int x)
{
	const int left = x - 5;
	const int right = x + 5;
	if (left<0)
	{
		return 5;
	}
	else if (right >=  gfx.ScreenWidth)
	{
		return gfx.ScreenWidth-6;
	}
	else
	{
		return x;
	}
}
int Game::ClampScreeny(int y)
{
	const int top = y - 5;
	const int bottom = y + 5;
	if (bottom >= gfx.ScreenHeight)
	{
		return gfx.ScreenHeight - 6;
	}
	else if (top <= 0)
	{
		return 5;
	}
	else
	{
		return y;
	}
}