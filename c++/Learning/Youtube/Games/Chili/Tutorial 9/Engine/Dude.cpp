#include "Dude.h"
#include"Graphics.h"
#include "Game.h"
#include"Graphics.h"
#include "MainWindow.h"





int Dude::ClampScreenx(int x, int width)
{
	const int right = x + width;
	if (x < 0)
	{
		return 0;
	}
	else if (right>= Graphics::ScreenWidth)
	{
		return (Graphics::ScreenWidth - 1) - width;
	}
	else
	{
		return x;
	}
}

int Dude::ClampScreeny(int y, int height)
{
	const int bottom = y + height;
	if (y < 0)
	{
		return 0;
	}
	else if (bottom >= Graphics::ScreenHeight)
	{
		return (Graphics::ScreenHeight - 1) - height;
	}
	else
	{
		return y;
	}
}

void Dude::Update()
{
	if (wnd.kbd.KeyIsPressed(VK_RIGHT))
	{
		X += 1;
	}
	if (wnd.kbd.KeyIsPressed(VK_LEFT))
	{
		X -= 1;
	}
	if (wnd.kbd.KeyIsPressed(VK_DOWN))
	{
		Y += 1;
	}
	if (wnd.kbd.KeyIsPressed(VK_UP))
	{
		Y -= 1;
	}

	ClampScreenx(X, Width);
	ClampScreeny(Y, Height);
}