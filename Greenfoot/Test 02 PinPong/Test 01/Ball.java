import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Ball here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ball extends Actor
{
    /**
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public Ball()
    {    
        GreenfootImage image = getImage();  
        image.scale(30, 30);
        setImage(image);
        setRotation(Greenfoot.getRandomNumber(360-0));
    }
    int Right=0;
    int Left=0;
    public void act() 
    {

        int Rotation=getRotation();
        move(2);
        Hit(Rotation);
        At_Top_Bottom(Rotation);
        Kill_The_Fucking_Bugs(Rotation);
        Score();
        
    }
    public void Hit(int Rotation)
    {
        Actor brick;
        brick=getOneObjectAtOffset(0,0,Brick.class);
       
        if (brick!=null)
        {
           
           if (Rotation<360 && Rotation>315)
           {
               setRotation(180+Math.abs(Rotation-360));
           }
           
           else if (Rotation<315 && Rotation>270)
           {
               setRotation(270-Math.abs(Rotation-360));
           }
           
           else if (Rotation>0 && Rotation<45)
           {
               setRotation(180-Rotation);
           }
           
           else if (Rotation>45 && Rotation<90)
           {
               setRotation(135-(Rotation-45));
           }
           else if (Rotation==0)
           {
               setRotation(180);
           }
           
           
           
           else if (Rotation>90 && Rotation<135)
           {
               setRotation(90-(Rotation-90));
           }
           
           else if (Rotation>135 && Rotation<180)
           {
               setRotation(Math.abs(Rotation-180));
           }
           
           else if (Rotation>180 && Rotation<225)
           {
               setRotation(-(Rotation-180));
           }
           
           else if (Rotation>225 && Rotation<270)
           {
               setRotation(315-(Rotation-225));
           }
           else if (Rotation==180)
           {
               setRotation(0);
           }
       }
    }
    
    public void At_Top_Bottom(int Rotation)
    {
        if (getY()<=8 )
        {
            
          if (Rotation>270 && Rotation<315)
           {
               setRotation(90-Math.abs(270-Rotation));
           }
           else if (Rotation>315 && Rotation<360)
           {
               setRotation(Rotation-315);
           }
           
           else if (Rotation>180 && Rotation<225)
           {
               setRotation(135+(Rotation-180));
           }
           
           else if (Rotation>225 && Rotation<270)
           {
               setRotation(180-(Rotation-225));
           }
        }
        
        
        if (getY()>=390)
        {
           if (Rotation>0 && Rotation<45)
           {
               setRotation(360-Rotation);
           }
           
           else if (Rotation>45 && Rotation<90)
           {
               setRotation(315-(Rotation-45));
           }
           
           else if (Rotation>90 && Rotation<135)
           {
               setRotation(270-(Rotation-90));
           }
           
           else if (Rotation>135 && Rotation<180)
           {
               setRotation(180+(Rotation-135));
           }
        }
    }
    
    public void Kill_The_Fucking_Bugs(int Rotation)
    {
       
    }
    
    public void Score()
    {
        if (getX()>=590)
        {
            Left++;
            Ball_Reset();
        }
        if (getX()<10)
        {
            Right++;
            Ball_Reset();
        }
        getWorld().showText(Integer.toString(Right),550,30);
        getWorld().showText(Integer.toString(Left),30,30);
        
    }
    public void Ball_Reset()
    {
        World world = getWorld();
        setLocation(world.getWidth()/2,world.getHeight()/2);
        setRotation(Greenfoot.getRandomNumber(360-0));
    }
}
