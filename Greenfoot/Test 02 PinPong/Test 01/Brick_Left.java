import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Brick_Left here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Brick_Left extends Brick
{
    /**
     * Act - do whatever the Brick_Left wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        Move();
    }    
    public void Move()
    {
        if (Greenfoot.isKeyDown("W"))
        {
            move(-4);
        }
        if (Greenfoot.isKeyDown("S"))
        {
            move(4);
        }
    }
}
