import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
        
        
    }

    /*
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Grapes grapes = new Grapes();
        addObject(grapes,358,30);

        Grapes grapes2 = new Grapes();
        addObject(grapes2,442,136);
        Grapes grapes3 = new Grapes();
        addObject(grapes3,419,305);
        Grapes grapes4 = new Grapes();
        addObject(grapes4,413,309);
        Grapes grapes5 = new Grapes();
        addObject(grapes5,378,332);
        Frog frog = new Frog();
        addObject(frog,224,75);
        Frog frog2 = new Frog();
        addObject(frog2,232,142);
        Frog frog3 = new Frog();
        addObject(frog3,275,245);
        Frog frog4 = new Frog();
        addObject(frog4,213,328);
        frog4.setLocation(209,306);
        frog3.setLocation(329,189);
        frog.setLocation(482,47);
        grapes4.setLocation(536,144);
        grapes3.setLocation(471,286);
        grapes5.setLocation(330,370);
        frog4.setLocation(200,261);
        frog3.setLocation(368,203);
        frog2.setLocation(259,101);
        Grapes grapes6 = new Grapes();
        addObject(grapes6,231,115);
        Grapes grapes7 = new Grapes();
        addObject(grapes7,203,236);
        Grapes grapes8 = new Grapes();
        addObject(grapes8,198,266);
        Grapes grapes9 = new Grapes();
        addObject(grapes9,214,297);
        Grapes grapes10 = new Grapes();
        addObject(grapes10,271,299);
        Grapes grapes11 = new Grapes();
        addObject(grapes11,306,250);
        Grapes grapes12 = new Grapes();
        addObject(grapes12,305,158);
        Grapes grapes13 = new Grapes();
        addObject(grapes13,312,48);
        Grapes grapes14 = new Grapes();
        addObject(grapes14,395,4);
        Grapes grapes15 = new Grapes();
        addObject(grapes15,391,123);
        Grapes grapes16 = new Grapes();
        addObject(grapes16,392,218);
        Grapes grapes17 = new Grapes();
        addObject(grapes17,400,301);
        Grapes grapes18 = new Grapes();
        addObject(grapes18,409,369);
        removeObject(grapes8);
        grapes9.setLocation(144,82);
        grapes10.setLocation(248,355);
        grapes14.setLocation(497,46);
        grapes2.setLocation(464,120);
        grapes15.setLocation(391,97);
        Frog frog5 = new Frog();
        addObject(frog5,492,275);
        Frog frog6 = new Frog();
        addObject(frog6,63,63);
        Frog frog7 = new Frog();
        addObject(frog7,83,384);
        Frog frog8 = new Frog();
        addObject(frog8,191,166);
        Frog frog9 = new Frog();
        addObject(frog9,175,40);
        Frog frog10 = new Frog();
        addObject(frog10,507,33);
        Frog frog11 = new Frog();
        addObject(frog11,364,159);
        Frog frog12 = new Frog();
        addObject(frog12,559,206);
        Frog frog13 = new Frog();
        addObject(frog13,368,66);
        Frog frog14 = new Frog();
        addObject(frog14,376,313);
        Frog frog15 = new Frog();
        addObject(frog15,284,338);
        frog8.setLocation(198,180);
        removeObject(frog8);
        removeObject(frog3);
        Fish_2 fish_2 = new Fish_2();
        addObject(fish_2,45,360);
        Fish_1 fish_1 = new Fish_1();
        addObject(fish_1,15,36);
        fish_1.setLocation(30,38);
        frog6.setLocation(130,156);
        frog7.setLocation(153,342);
        frog4.setLocation(175,252);
        frog14.setLocation(368,283);
        frog15.setLocation(272,295);
    }
}
