import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random; 

/**
 * Write a description of class Frog here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Frog extends Actor
{
    /**
     * Act - do whatever the Frog wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
   
    public void act() 
    {
        
        Random rand = new Random();
        int  Rand_Num = rand.nextInt(7) + 1;
        if (Rand_Num< 4)
        {
            move(5);
        }
        else if (Rand_Num== 5)
        {
            turn(5);
        }
        else if (Rand_Num== 6)
        {
            turn(15);
        }
        else if (Rand_Num== 7)
        {
            turn(10);
        }
        Eat();
    }
    public void Eat()
    {
        Actor fish_1;
        fish_1=getOneObjectAtOffset(0,0,Fish_1.class);
        if (fish_1!=null)
        {
            World world;
            world=getWorld();
            world.removeObject(fish_1);
            
            
        }
        Actor fish_2;
        fish_2=getOneObjectAtOffset(0,0,Fish_2.class);
        if (fish_2!=null)
        {
            World world;
            world=getWorld();
            world.removeObject(fish_2);
            
            
        }
    }
}
