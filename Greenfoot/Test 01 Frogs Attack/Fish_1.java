import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Fish_1 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Fish_1 extends Actor
{
    int Eaten;
    /**
     * Act - do whatever the Fish_1 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
   {
        Move();
        Eat();
        getWorld().showText("Yellow = "+Integer.toString(Eaten),100 ,20);
        if (Greenfoot.isKeyDown("CONTROL"))
        {
         move(5);   
        }
    }
    
    public void Eat()
    {
        Actor grapes;
        grapes=getOneObjectAtOffset(0,0,Grapes.class);
        if (grapes!=null)
        {
            World world;
            world=getWorld();
            world.removeObject(grapes);
            Eaten++;
            
        }
    }
    public void Move()
        {
            if (Greenfoot.isKeyDown("UP"))
            {
                move(5);
            };
            if (Greenfoot.isKeyDown("DOWN"))
            {
                move(-5);
            };
            if (Greenfoot.isKeyDown("LEFT"))
            {
                turn(-5);
            };
            if (Greenfoot.isKeyDown("RIGHT"))
            {
                turn(5);
            };
        } 

}
