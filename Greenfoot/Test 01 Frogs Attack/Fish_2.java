import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Fish_2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Fish_2 extends Actor
{
    /**
     * Act - do whatever the Fish_2 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    int Eaten =0;
    public void act() 
    {
        Move();
        Eat();
        getWorld().showText("Green = "+Integer.toString(Eaten),500 ,20);
        if (Greenfoot.isKeyDown("SPACE"))
        {
         move(5);   
        }
    }  
    public void Eat()
    {
        Actor grapes;
        grapes=getOneObjectAtOffset(0,0,Grapes.class);
        if (grapes!=null)
        {
            World world;
            world=getWorld();
            world.removeObject(grapes);
            Eaten++;
        }
    }
    public void Move()
        {
            if (Greenfoot.isKeyDown("W"))
            {
                move(5);
            };
            if (Greenfoot.isKeyDown("S"))
            {
                move(-5);
            };
            if (Greenfoot.isKeyDown("A"))
            {
                turn(-5);
            };
            if (Greenfoot.isKeyDown("D"))
            {
                turn(5);
            };
        }
}
