import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Rock here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rock extends Actor
{
    /**
     * Act - do whatever the Rock wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Rock()
    {
     turn(90);
     
    }
    int Act=0;
    int Sec=1;
    public void act() 
    {
        
        Act++;
        if (Act==55)
        {
         Act=0;
         Sec+=1;
         System.out.println(Act);
        }
        
        if (Sec%5==0)
        {
         System.out.println("t");
         Sec=1;
         New();
         
        }
        move(3);
        Hit();
        Remove();
        

    }
    public void New()
    {
        System.out.println("tets");
        
        getWorld().addObject(new Rock(),Greenfoot.getRandomNumber(395),0);
    }
    
    public void Remove()
    {
        int y = getY();
        if (y==getWorld().getHeight())
        {
            getWorld().removeObject(this);
        }
    }
    
    public void Hit()
    {
     Actor rocket;
     rocket = getOneObjectAtOffset(0, 0, Rocket.class);
     if (rocket!=null)
     {
         getWorld().removeObject(rocket);
     }
        
    }   
}
