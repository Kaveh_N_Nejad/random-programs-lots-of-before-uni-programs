import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */

    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(400, 650, 1); 
        Prepare();

    }

    public void Prepare()
    {
        Rocket rocket = new Rocket();
        addObject(rocket,getWidth()/2,500);
        Rock rock = new Rock();
        addObject(rock,345,6);
        Rock rock2 = new Rock();
        Rock rock3 = new Rock();
        Rock rock4 = new Rock();
        Rock rock5 = new Rock();
        
    }

}
