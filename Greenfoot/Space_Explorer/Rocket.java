import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Rocket here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rocket extends Actor
{
    /**
     * Act - do whatever the Rocket wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Rocket()
    {
      turn(-90);  
    }
    public void act() 
    {
        Move();
    }   
    
    public void Move()
    {
     int x = getX();
     int y = getY();
     if (Greenfoot.isKeyDown("RIGHT"))
     {
         setLocation(x+2,y);
     }
     
     if (Greenfoot.isKeyDown("LEFT"))
     {
         setLocation(x-2,y);
     }
    }
}
